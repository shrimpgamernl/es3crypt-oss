# ES3Crypt

This tool allows you decrypt/encrypt EasySave3 save files. It also allows hosting a web frontend using *layout files*, for an example see [`/layouts/Phasmophobia.cppi`](/layouts/Phasmophobia.cppi).
